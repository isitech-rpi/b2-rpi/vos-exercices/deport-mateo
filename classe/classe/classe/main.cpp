//
//  main.cpp
//  classe
//
//  Created by Matéo Deport on 03/07/2024.
//

#include <iostream>
#include "classe.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    //static
    Cours matiere_1("c++", "b2-RPI");
    cout << "la matière est : " << matiere_1.getMatiere() << " et la promo est : " << matiere_1.getPromo() << "\n";
    
    //Dynamique
    Cours* matiere_2;
    matiere_2 = new Cours("PHP","3olen");
    cout << "la matière 2 est : " << matiere_2->getMatiere() << " et la promo 2 est : " << matiere_2->getPromo() << "\n";
    
    //tableau static
    int Taille_lesCours_1 = 2;
    Cours lesCours_1[2] = {Cours("UWP","B3-WTech"),Cours("UML","b2-WTech")};
    cout << "les premier cours : ";
        for (int i = 0; i < Taille_lesCours_1; ++i)
        {
            cout << lesCours_1[i].getMatiere() << " "<< lesCours_1[i].getPromo() << " ";
        }
    cout << "\n";
    
    //tableau dynamique
    int Taille_lesCours_2 = 2;
    Cours CoursGit = Cours("Git","3olen") ;
    Cours CoursPHP = Cours("PHP","B2-WTech");
    Cours* lesCours_2 = new Cours[Taille_lesCours_2];
    
    lesCours_2[0] = CoursGit;
    lesCours_2[1] = CoursPHP;
    
    for(int i = 0; i < Taille_lesCours_2; ++i) {
            cout << lesCours_2[i].getMatiere() << " " << lesCours_2[i].getPromo() << "\n";
    }
    
    lesCours_1[0].setPromo("B1 - DEV");
    lesCours_2[1].setPromo("B2 - DEV");
    
    delete[] lesCours_2;
    delete matiere_2;
    
}
