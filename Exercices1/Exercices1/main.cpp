//
//  main.cpp
//  Exercices1
//
//  Created by Matéo Deport on 01/07/2024.
//

#include <iostream>
#include "fonction.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    //Les Variables
    // Exercices 1.1
    int a = 1;
    int b = 5;
    float f = 3.14;
    char c = 'C';
    char d;
    
    cout << a << " a pour adresse hexadécimal : " << &a << "\n";
    cout << b << " a pour adresse hexadécimal : " << &b << "\n" ;
    cout << f << " a pour adresse hexadécimal : " << &f << "\n" ;
    cout << c << " a pour adresse hexadécimal : " << &c << "\n" ;
    
    d = 'D';
    cout << d << " a pour adresse hexadécimal : " << &d << "\n";
    
    a = 5 ;
    b = 1 ;
    cout << "la valeur de a est " << a << "\n";
    cout << "la valeur de b est " << b << "\n";
    
    a = 3.14;
    f = 1;
    cout << "la valeur de a est " << a << "\n";
    cout << "la valeur de f est " << f << "\n";
    
    a = f;
    cout << "la valeur de f est " << f << " la valeur a est "<< a << "\n";
    
    d = 90;
    cout << "la valeur de d est " << static_cast<int>(d) << "\n";
    d = d + 255;
    cout << "la valeur de d est " << static_cast<int>(d) << "\n";
    
    //Exercices 1.2
    
    int i1 = 1;
    int i2 = 3;
    int r1;
    
    r1= i1 / i2;
    cout << "la valeur de r1 est " << r1 << "\n";
    
    float d1 = 1, d2 = 3, r2;
    
    r2 = i1 / i2;
    cout << "la valeur de r2 est " << r2 << "\n";
    
    r2= d1/i2;
    cout << "la valeur de r2 est " << r2 << "\n";
    
    r2 = i1/d2;
    cout << "la valeur de r2 est " << r2 << "\n";
    
    r2= d1 / d2;
    cout << "la valeur de r2 est " << r2 << "\n";
    
    //Exercices 1.3
    
    int A = 3;
    int B = 10;
    int* p;
    
    p = &B;
    
    cout << "la valeur de p est : " << p << " sont adresse est : " << &p << " et sa valeur pointé est : " << *p << "\n";
    
    p = &A;
    int ValeurX2 = *p * 2;
    
    cout << "la valeur pointé multiplier par 2 de p est : " << ValeurX2 << "\n";
    
    *p = *p +1;
    cout << "la valeur de p +1 est : " << *p << "\n";
    
    // Exercices 2
    // Exercices 2.1
    
    int heures, minutes;
    
    cout << "veuillez saisir une heure \n";
    cin >> heures;
    /*while (!(cin >> heures) || heures > 23)
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << endl << "!! Saisie incorrecte, veuillez saisir une heure. \n";
    }*/
    
    cout << "veuillez saisir les minutes \n";
    cin >> minutes;
    /*while (!(cin >> minutes) || minutes > 59)
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << endl << "!! Saisie incorrecte, veuillez saisir des minutes. \n";
    }*/
    
   if (minutes == 59 && heures != 23)
   {
       heures += 1;
       minutes = 00;
   }
   else if (heures == 23 && minutes == 59)
   {
       heures = 00;
       minutes = 00;
   }
   else if (heures > 23 || minutes >59)
   {
       
       cout << endl << "!! Saisie incorrecte, veuillez saisir une heures ou des minutes correctes. \n";
   }
   else
   {
       minutes += 1;
   }

    
    cout << "dans une minutes il seras " << heures <<":" << minutes << "\n";
    
    //Excercices 2.2
    
    int age;
    cout << "veuillez saisir l'age de la personne \n";
    cin >> age;
    /*while (!(cin >> age))
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << endl << "!! Saisie incorrecte, veuillez saisir un age. \n";
    }*/
    
    if (age < 6)
    {
        cout << "hors catégorie"<< endl;
    }
    switch (age){
        case 6:
        case 7:
        case 8:
        case 9:
            cout << "catégorie : pré-poussin"<< endl;
            break;
        case 10 :
        case 11 :
            cout << "catégorie : poussin"<< endl;
            break;
        case 12:
        case 13:
            cout << "catégorie : benjamin"<< endl;
            break;
        case 14:
        case 15:
            cout << "catégorie : minime"<< endl;
            break;
        case 16:
        case 17:
            cout << "catégorie : cadet"<< endl;
            break;
        default:
            cout << "catégorie : senior"<< endl;
    }
    
    //Exercices2.3
    
    int note;
    int NombreNote = 0;
    float MoyenneFinal;
    int sum = 0;
    
    do {
        cout << "Entrer une note ? \n";
        cin >> note;
        sum = sum + note;
        NombreNote = NombreNote + 1;
    } while (note != (-1));
    MoyenneFinal = static_cast<float>(sum) / static_cast<float>(NombreNote);
    cout << "Voici votre moyenne " << MoyenneFinal <<"\n";
    
    //Exercices 3
    // Exercice 3.1
    saisirEntierEntre(1,20);
    
    
    // Exercice 3.2
    int choixJeux;
    cout << "choisis un jeux de carte entre 32 et 54 \n";
    cin >> choixJeux;
    tireUneCarte(choixJeux);
    
    
    //Exercice 3.3
    char Valeur1, Valeur2;
    cout << "selectionner 2 valeur a permuter \n";
    cout << "valeur 1 : ";
    cin >> Valeur1;
    cout << "valeur 2 : ";
    cin >> Valeur2;
    PermuterContenue(Valeur1,Valeur2);
    
    
    //Exercices Tableau 1
    //Tableau 1.1
    const int TailleTableau = 5;
    int Tableau[TailleTableau] = {};
    remplirTab(Tableau, TailleTableau);
    
    cout << "Tableau rempli : ";
        for (int i = 0; i < TailleTableau; ++i)
        {
            cout << Tableau[i] << " ";
        }
    
    //Tableau 1.2
    
    copie(Tableau, TailleTableau);
    
    //Tableau 1.3
    
    bool tri;
    cout << "si vous voulez tri par ordre croissant metter 'true' ou si vous decroissant metter 'false' \n";
    
    cin >>tri;
    sortTab(Tableau,tri, TailleTableau);
    
    return 0;
}
