//
//  classe.hpp
//  Exercices1
//
//  Created by Matéo Deport on 02/07/2024.
//

#ifndef classe_hpp
#define classe_hpp

#include <string>

class Cours
{
private:
    std::string matiere;
    std::string promo;
    
public:
    Cours();
    Cours(std::string matiere, std::string promo);
    
    ~Cours();
    
    std::string getMatiere();
    std::string getPromo();
    
    std::string setPromo(std::string promo);
    std::string setMatiere(std::string matiere);
    
    void Enseigner();
    
    std::string getAdresse(std::string attribut);
};


#endif /* classe_hpp */
