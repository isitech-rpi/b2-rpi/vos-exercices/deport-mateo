//
//  fonction.cpp
//  Exercices1
//
//  Created by Matéo Deport on 02/07/2024.
//

#include <iostream>
#include "fonction.hpp"

using namespace std;

//3.1
int saisirEntierEntre(int min, int max)
{
    int NbSaisie;
    std::cout << "veuillez saisir un entier \n";
    std::cin >> NbSaisie;
    if (NbSaisie > min && NbSaisie < max)
    {
        std::cout << "la saisie est conforme \n";
    }
    else {
        std::cout << "la saisie est non conforme \n";
    }
    return 0;
}


//3.2
char tireUneCarte(int TypeJeux)
{
    
    const char *JeuxCartes32[] = {
        "7_coeur", "8_coeur", "9_coeur", "10_coeur", "valet_coeur", "dame_coeur", "roi_coeur", "as_coeur",
        "7_pique", "8_pique", "9_pique", "10_pique", "valet_pique", "dame_pique", "roi_pique", "as_pique",
        "7_carreau", "8_carreau", "9_carreau", "10_carreau", "valet_carreau", "dame_carreau", "roi_carreau", "as_carreau",
        "7_trèfle", "8_trèfle", "9_trèfle", "10_trèfle", "valet_trèfle", "dame_trèfle", "roi_trèfle", "as_trèfle"
    };
    
    const char *JeuxCartes54[] = {
        "2_coeur", "3_coeur", "4_coeur", "5_coeur", "6_coeur", "7_coeur", "8_coeur", "9_coeur", "10_coeur",
        "valet_coeur", "dame_coeur", "roi_coeur", "as_coeur",
        "2_pique", "3_pique", "4_pique", "5_pique", "6_pique", "7_pique", "8_pique", "9_pique", "10_pique",
        "valet_pique", "dame_pique", "roi_pique", "as_pique",
        "2_carreau", "3_carreau", "4_carreau", "5_carreau", "6_carreau", "7_carreau", "8_carreau", "9_carreau", "10_carreau",
        "valet_carreau", "dame_carreau", "roi_carreau", "as_carreau",
        "2_trèfle", "3_trèfle", "4_trèfle", "5_trèfle", "6_trèfle", "7_trèfle", "8_trèfle", "9_trèfle", "10_trèfle",
        "valet_trèfle", "dame_trèfle", "roi_trèfle", "as_trèfle",
        "joker_noir", "joker_rouge"
    };
    std::srand(static_cast<unsigned int>(std::time(0)));
    
    if (TypeJeux == 54)
    {
        int indexAleatoire = rand() % (sizeof(JeuxCartes54) / sizeof(JeuxCartes54[0]));
        std::cout << "Voici votre carte aleatoire : " << JeuxCartes54[indexAleatoire] << "\n";
    }
    else if(TypeJeux == 32)
    {
        int indexAleatoire = rand() % (sizeof(JeuxCartes32) / sizeof(JeuxCartes32[0]));
        std::cout << "Voici votre carte aleatoire : " << JeuxCartes32[indexAleatoire] << "\n";
    }
    else
    {
        std::cout << "aucun choix de carte fais" << "\n";
    }
    
    return 0;
}
    //3.3
    
char PermuterContenue(char Variable1, char Variable2)
{
        char AncienneVariable1 = Variable1;
        char AncienneVariable2 = Variable2;
        
        Variable1 = AncienneVariable2;
        Variable2 = AncienneVariable1;
        
        std::cout << "La variable 1 est maintenant : " << Variable1 <<"\n";
        std::cout << "La variable 2 est maintenant : " << Variable2 <<"\n";
        

    
    return 0;
}


void remplirTab(int Tableau[], int TailleTableau)
{
    int item;
    int index =0;
    do
    {
        cout << "saisissez un nombre a ajouter au tableau \n";
        cin >> item;
        if (item !=-1)
        {
            if (index < TailleTableau)
            {
                Tableau[index]=item;
                index++;
            }
            else{
                cout << "Tableau complet \n";
                item = -1;
            }
        }
    }
    while (item != (-1));
}

int* copie(int Tableau[],int TailleTableau)
{
    int* ptr_tableau;
    int* ptr_new_tableau;
    
    ptr_tableau = Tableau;
    ptr_new_tableau = new int[TailleTableau];
    
    cout << "copie du tableau : [";
    for (int i =0;i < TailleTableau; i++)
    {
        ptr_new_tableau[i] = Tableau[i];
        cout << ptr_new_tableau[i] << " ";
    }
    cout << "]"<< "\n";
    
    delete ptr_new_tableau;
    
    return 0;
}

void sortTab(int Tableau[], bool tri, int TailleTableau)
{
    if (tri)
    {
        sort(Tableau, Tableau + TailleTableau);
    }else{
        sort(Tableau, Tableau + TailleTableau, greater<int>());
    }
    
    
    cout << "voici le tableau tri par ";
    if (tri){
        cout <<"ordre croissant [";
    }
    else{
        cout <<"ordre décroissant [";
    }
    for (int i = 0; i < TailleTableau; ++i) {
        cout << Tableau[i] << " ";
    }
    cout << "] \n";
    
}
