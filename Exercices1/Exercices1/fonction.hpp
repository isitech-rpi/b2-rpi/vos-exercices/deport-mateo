//
//  fonction.hpp
//  Exercices1
//
//  Created by Matéo Deport on 02/07/2024.
//

#ifndef fonction_hpp
#define fonction_hpp

#include <stdio.h>



int saisirEntierEntre(int min, int max);

char tireUneCarte(int TypeJeux);

char PermuterContenue(char Variable1, char Variable2);

void remplirTab(int Tableau[], int TailleTableau);

int* copie(int Tableau[],int TailleTableau);

void sortTab(int Tableau[], bool tri, int TailleTableau);
#endif /* fonction_hpp */
