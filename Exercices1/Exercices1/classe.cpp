//
//  classe.cpp
//  Exercices1
//
//  Created by Matéo Deport on 02/07/2024.
//

#include "classe.hpp"
#include <iostream>
#include <string>

using namespace std;

Cours::Cours() : Cours("Math","6eme")
{
}

Cours::Cours(std::string matiere, std::string promo)
{
    this->matiere = matiere;
    std::cout << "l'adresse de la matiere est : " << this << " et sa valeur est : " << matiere;
    this->promo = promo;
    std::cout << "l'adresse de la promo est : " << this << " et sa valeur est : " << promo;
}

virtual Cours::~Cours() {
    this->setPromo("");
    this->setMatiere("");
}

std::string Cours::getPromo()
{
    return this->promo;
}

std::string Cours::getMatiere()
{
    return this->matiere;
}

std::string Cours::setPromo(std::string promo)
{
    this->promo = promo;
    return this->promo;
}

std::string Cours::setMatiere(std::string matiere)
{
    this->matiere = matiere;
    return this->matiere;
}

void Cours::Enseigner(){
    std::cout << "enseigne " << this->matiere << " à la promotion " << this->promo;
}

