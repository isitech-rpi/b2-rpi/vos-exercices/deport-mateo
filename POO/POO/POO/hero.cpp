//
//  hero.cpp
//  POO
//
//  Created by Matéo Deport on 03/07/2024.
//

#include "hero.hpp"
#include <iostream>
#include <string>

using namespace std;

namespace Personnage {
Heros::Heros()
{}

Heros::Heros(string nom, int niveau) : Personnage(nom)
{
    this->niveau = niveau;
}

Heros::Heros(string nom, string nomPouvoirs) : Personnage(nom)
{
    this->lesPouvoirs = Pouvoir::Pouvoir() = nomPouvoirs;
}

Heros::~Heros()
{}
}

