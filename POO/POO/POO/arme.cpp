//
//  arme.cpp
//  POO
//
//  Created by Matéo Deport on 04/07/2024.
//

#include "arme.hpp"
#include <iostream>
#include <string>

using namespace std;

namespace Arme {
Arme::Arme()
{
}

Arme::Arme(double attaque, double defense,int portee)
{
    this->puissanceAttaque = attaque;
    this->puissanceDefense = defense;
    this->portee = portee;
}


Arme::~Arme()
{
}

double Arme::Utilise(bool attaque)
{
    if(attaque)
    {
        cout << "attribut de l'arme en attaque : " << this->puissanceAttaque;
        return this->puissanceAttaque;
    }
    else{
        cout << "attribut de l'arme en defense : " << this->puissanceDefense;
        return this->puissanceDefense;
    }
}

double Arme::getAttaque()
{
    return this->puissanceAttaque;
}

double Arme::getDefense()
{
    return this->puissanceDefense;
}

}


