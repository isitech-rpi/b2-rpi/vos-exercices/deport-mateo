//
//  hero.hpp
//  POO
//
//  Created by Matéo Deport on 03/07/2024.
//

#ifndef hero_hpp
#define hero_hpp

#include <string>
#include "POO.hpp"
#include "pouvoir.hpp"

namespace Personnage {

class Heros : public Personnage
{
protected:
    Pouvoir::Pouvoir* lesPouvoirs;
public:
    Heros();
    Heros(string nom, int niveau = 2);
    Heros(string nom, string nomPouvoirs);
    ~Heros();
};
}
#endif /* hero_hpp */
