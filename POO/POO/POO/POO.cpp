//
//  POO.cpp
//  POO
//
//  Created by Matéo Deport on 03/07/2024.
//

#include "POO.hpp"
#include <iostream>
#include <string>

using namespace std;
namespace Personnage {
    Personnage::Personnage()
    {
    }

    Personnage::Personnage(string nom)
    {
        this->nom = nom;
    }
    
    Personnage::~Personnage()
    {
        
    }

    
    string Personnage::getNom()
    {
        return this->nom;
    }

    double Personnage::getVie()
    {
        return this->vie;
    }
    
    int Personnage::getNiveau()
    {
        return this->niveau;
    }

    bool Personnage::EstVivant()
    {
        return this->vie > 0 ;
    }

    bool Personnage::Attaquer(Personnage* ennemi)
    {
        if (ennemi->EstVivant())
        {
            if (!(armecourante == nullptr)){
                double attaque = this->armecourante->Utilise(true);
                double viePerdue = (this->vie + attaque) * this->niveau / 20.0;
                ennemi->vie -= viePerdue;
                if (ennemi->vie < 0) {
                    ennemi->vie = 0;
                }
                ennemi->Defendre(this);
                return ennemi->EstVivant();
            }
            else {
                double viePerdue = (ennemi->vie) * this->niveau / 20.0;
                ennemi->vie -= viePerdue;
                if (ennemi->vie < 0) {
                    ennemi->vie = 0;
                }
                ennemi->Defendre(this);
                return ennemi->EstVivant();
            }
        }
        return false;
    }

    bool Personnage::Defendre(Personnage* attaquant)
    {
        if (this->EstVivant())
        {
            if (!(armecourante == nullptr)){
                double defense = this->armecourante->Utilise(false);
                double viePerdue = (this->vie + defense) * this->niveau / 20.0;
                attaquant->vie -= viePerdue;
                if (attaquant->vie < 0) {
                    attaquant->vie = 0;
                }
                return attaquant->EstVivant();
            }
            else {
                double viePerdue = attaquant->vie * this->niveau / 20.0;
                attaquant->vie -= viePerdue;
                if (attaquant->vie < 0) {
                    attaquant->vie = 0;
                }
                return attaquant->EstVivant();
            }
        }
        return false;
    }

    void Personnage::ColecterArme(Arme::Arme* arme)
    {
        this->armecourante = arme;
    }


    
    

}

