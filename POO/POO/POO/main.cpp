//
//  main.cpp
//  POO
//
//  Created by Matéo Deport on 03/07/2024.
//

#include <iostream>
#include "POO.hpp"
#include "hero.hpp"
#include "arme.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    // Static instantiation of Personnage
    Personnage::Personnage personnageTest1("Guethenoc");
    
    cout << "Les informations du personnage sont : \n"
         << personnageTest1.getNom() << " "
         << personnageTest1.getVie() << " "
         << personnageTest1.getNiveau() << "\n";
    
    // Dynamic instantiation of Personnage
    Personnage::Personnage* personnageTest2 = new Personnage::Personnage("Balcmeg");
    
    cout << "Les informations du personnage sont : \n"
         << personnageTest2->getNom() << " "
         << personnageTest2->getVie() << " "
         << personnageTest2->getNiveau() << "\n";
    
    // Static instantiation of Heros
    Personnage::Heros herosTest1("Gollum");
    
    cout << "Les informations du héros sont : \n"
         << herosTest1.getNom() << " "
         << herosTest1.getVie() << " "
         << herosTest1.getNiveau() << "\n";
    
    // Dynamic instantiation of Heros
    Personnage::Heros* herosTest2 = new Personnage::Heros("Frodon", 10);
    
    cout << "Les informations du héros sont : \n"
         << herosTest2->getNom() << " "
         << herosTest2->getVie() << " "
         << herosTest2->getNiveau() << "\n";
    
    // Static instantiation of Arme
    Arme::Arme arme1(20.0, 2.0, 3);
    
    // Dynamic instantiation of Arme
    Arme::Arme* arme2 = new Arme::Arme(2.0, 3.0, 4);
    
    // Assign arme1 to Guethenoc and Gollum
    personnageTest1.ColecterArme(&arme1);
    herosTest1.ColecterArme(&arme1);
    
    // Assign arme2 to Frodon
    herosTest2->ColecterArme(arme2);
    
    // Guethenoc attaque Balcmeg
    personnageTest1.Attaquer(personnageTest2);
    cout << "\nAprès l'attaque de Guethenoc sur Balcmeg:\n";
    cout << personnageTest1.getNom() << " a pour vie " << personnageTest1.getVie() << "\n";
    cout << personnageTest2->getNom() << " a pour vie " << personnageTest2->getVie() << "\n";
    
    // Frodon attaque Gollum
    herosTest2->Attaquer(&herosTest1);
    cout << "\nAprès l'attaque de Frodon sur Gollum:\n";
    cout << herosTest1.getNom() << " a pour vie " << herosTest1.getVie() << "\n";
    cout << herosTest2->getNom() << " a pour vie " << herosTest2->getVie() << "\n";
    

    return 0;
}
