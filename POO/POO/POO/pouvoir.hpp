//
//  pouvoir.hpp
//  POO
//
//  Created by Matéo Deport on 04/07/2024.
//

#ifndef pouvoir_hpp
#define pouvoir_hpp

#include <stdio.h>
#include <string>

using namespace std;

namespace Pouvoir {

class Pouvoir{
protected:
    string nom;
    
public:
    Pouvoir(string nom);
    ~Pouvoir();
    
    void Invoque();
};



}





#endif /* pouvoir_hpp */
