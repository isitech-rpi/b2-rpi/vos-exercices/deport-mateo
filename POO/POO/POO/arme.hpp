//
//  arme.hpp
//  POO
//
//  Created by Matéo Deport on 04/07/2024.
//

#ifndef arme_hpp
#define arme_hpp

#include <stdio.h>

namespace Arme {
class Arme
{
protected:
    double puissanceAttaque = 0;
    double puissanceDefense = 0;
    int portee = 0;
    
public:
    Arme();
    Arme(double attaque, double defense,int portee);
    
    ~Arme();
    
    double Utilise(bool attaque);
    
    double getAttaque();
    double getDefense();
};
}


#endif /* arme_hpp */
