//
//  pouvoir.cpp
//  POO
//
//  Created by Matéo Deport on 04/07/2024.
//

#include "pouvoir.hpp"
#include <string>
#include <iostream>

using namespace std;

namespace Pouvoir {

Pouvoir::Pouvoir(string nom)
{
    this->nom = nom;
}

Pouvoir::~Pouvoir()
{}

void Pouvoir::Invoque()
{
    cout << "le pouvoir : " << this->nom << " a été invoqué.";
}

}
