//
//  POO.hpp
//  POO
//
//  Created by Matéo Deport on 03/07/2024.
//

#ifndef POO_hpp
#define POO_hpp

#include <string>
#include "arme.hpp"

using namespace std;

namespace Personnage {
class Personnage
{
protected:
    double vie = 10.0;
    int niveau = 1;
    bool Defendre(Personnage* attaquant);
    Arme::Arme* armecourante = nullptr;
        
private:
    string nom = "";
        
public:
    Personnage();
    Personnage(string nom);
    
    ~Personnage();
        
    string getNom();
    double getVie();
    int getNiveau();
    
    bool Attaquer(Personnage* ennemi);
    
    bool EstVivant();
    
    void ColecterArme(Arme::Arme* arme);
    
    Arme::Arme* getarme();
    
    
};

}


#endif /* POO_hpp */
